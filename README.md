# Vagrant Crash Course

Notes from the Vagrant Crash Course YouTube video.

---

# Source

[Vagrant Crash Course](https://www.youtube.com/watch?v=vBreXjkizgo&t=0s&list=PLx9eaj7kEtwxVM_KPOC_K877_UdIBxYSJ&index=5) 

# Notes

- The great thing about technologies like Vagrant and Docker is that you have these isolated environments that you can easily share with others.
- Video focused on development, not deployment.
- Getting started
	- Install [Vagrant](https://www.vagrantup.com/downloads.html) & [VirtualBox](https://www.virtualbox.org)
		- Vagrant needs a provider, VitualBox is the most popular provider.
		- When using Vagrant you will rarely use the provider UI itself, you want to manage things through your shell with Vagrant. It can be useful for viewing the status.
- Vagrantfile
	- Core of Vagrant, define configuration, IP addresse, providers, etc.
	- Five main parts
		- `config.vm.box` - Operating System. We're using `ubuntu/trusty64` for this tutorial.
		- `config.vm.provider` - In our case, VirtualBox. 
		- `config.vm.network` - How your host sees your virtual machine.
		- `config.vm.synced_folder` - How you access files from your computer. Easier than using SSH for everything.
		- `config.vm.provision` - What we want setup (LAMP, Node, etc.)
	- Written in Ruby but it's just a configuration file, you don't need to know Ruby
- To find predefined Vagrant boxes you can use [Discover Vagrant Boxes](https://app.vagrantup.com/boxes/search).
- Once you have the box you want to use `cd` into your working directory and use `vagrant init ubuntu/trusty64` to create a Vagrantfile. 
- ⤷ There are Visual Studio Code extensions that make working with Vagrant better.
- Vagrantfile:
	- You cant get complete configuration documentation in [Vagrant's Documentation](https://docs.vagrant.com).
	- We're going to remove or replace most of the commented descriptions. He also moves the provider settings to directly under the box settings.
	- Vagrant configuration version, don't change this unless you know what you're doing.

```ruby
Vagrant.configure("2") do |config|
```

	- Your operating system:

```ruby
# Box Settings
config.vm.box = "ubuntu/trusty64"
```

	- Provider settings may be unnecessary since VirtualBox is the default but we'll keep it for further configuration and remove the presets:

```ruby
# Provider Settings
config.vm.provider "virtualbox" do |vb|

end
```

	- These two settings are enough to create the virtual machine, we'll return to make further settings.
- Basic Vagrant commands 
	- `vagrant up` - Creates or starts the box.
		- When you run this for the first time Vagrant will download the operating system (if you don't already have it installed), do the basic configuration for things like SSH but without provisioning won't do anything more that create a basic Ubuntu server. 
		- If the box already exists it will start it running.
	- `vagrant destroy` - Deletes the box.
	- `vagrant suspend` - Suspend the box state to disk.
	- `vagrant resume` - Restore it from disk.
- Returning to Vagrantfile we'll give the box more system resources.
	- Within the provider settings add memory and CPU:

```ruby
# Provider Settings
config.vm.provider "virtualbox" do |vb|
	vb.memory = 2048
	vb.cpus = 4
end
```

- Commands
	- `vagrant reload` - Reloads the box with the updated system specifications we configured.
	- `vagrant ssh` - SSH into our new box.
		- You _could_ setup the box via SSH but you usually want to do setup via provisions in the Vagrantfile.
		- Doing some manually:
			- Nothing is setup, no Apache so `ls /var/www/html` will show no files.
			- Update **apt**: `sudo apt update`
			- Install **git**: `sudo apt install -y git`
			- Install **Apache**: `sudo apt install -y apache2`
			- Confirm it's installed, `ls /var/www/html` should now show an `index.html` file.
- Web server
	- We can't actually use this as a server yet from a browser because we haven't setup network settings.
		- Update the Vagrantfile's network settings
			- Port `80` is the default HTTP port for Apache and we want to forward that port to our host's `8080` port.

```ruby
# Network Settings
config.vm.network "forwarded_port", guest: 80, host: 8080
# config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"
# config.vm.network "private_network", ip: "192.168.33.10"
# config.vm.network "public_network"
```

	- Now exit SSH `exit` and reload the box `vagrant reload`
	- Open a browser and go to `localhost:8080`. The Apache2 Ubuntu Default Page should appear.
	- A better way to do this however would be to use a private network IP instead of localhost.

```ruby
# Network Settings
# config.vm.network "forwarded_port", guest: 80, host: 8080
# config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"
config.vm.network "private_network", ip: "192.168.33.10"
# config.vm.network "public_network"
```

	- Reload and in a browser navigate to: http://192.168.33.10/
	- We can make this a bit easier by editing our **hosts** file and giving it a host name.
		- On Mac or Linux open the `/etc/hosts` file in a text editor.
		- Append the IP and give it a host name 

```
# Vagrant Servers
192.168.33.10 vgdemo.local www.vgdemo.local
```

	- Now we can use http://vgdemo.local/ in our browser.
	- Returning to SSH
		- Open the `index.html` file to make a change: `sudo nano /var/www/html/index.html`
		- Change the title to `Hello World`
		- Save with `^ + X`, `Y` and `↵`
	- Reload browser and the title should now be `Hello World`.
- That was the long way to do it, it would be easier just to use the sync folder.

```ruby
# Folder Settings
config.vm.synced_folder ".", "/var/www/html"
```

	- The first parameter is where the host folder should exist and we want it just in our working vagrant folder so it should be `.`. This literally syncs the current Vagrant directory so opening vgdemo.local in the browser will display the directory including the Vagrantfile since index.html doesn't exist.
	- The second parameter is the directory on the vm, in this case we want to change the the web server contents so `/var/www/html`
- Reload Vagrant box.
- Create a new `index.html`, add something basic:

```html
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Hello World</title>
	</head>
	<body>
		<h1>Hello World</h1>
	</body>
</html>
```

- Reload browser, hello world.
- We can now make things a little more secure by changing the mount options:

```ruby
# Network Settings
# config.vm.network "forwarded_port", guest: 80, host: 8080
# config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"
config.vm.network "private_network", ip: "192.168.33.10", :mount_options => ["dmode=777", "fmode=666"]
# config.vm.network "public_network"
```
 
- To improve network performance with nfs systems that support it we can further change it to:

```ruby
# Network Settings
# config.vm.network "forwarded_port", guest: 80, host: 8080
# config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"
config.vm.network "private_network", ip: "192.168.33.10", :nfs => { :mount_options => ["dmode=777", "fmode=666"] }
# config.vm.network "public_network"
```

- Moving on to provisions.
	- Start by destroying the box `vagrant destroy` and `y`y
	- This deletes the box but leaves the Vagrantfile, metadata and synced folder intact.
	- Now we can uncomment the provisions, anything you put in it will be run on the box.

```ruby
# Provision Settings
config.vm.provision "shell", inline: <<-SHELL
	apt-get update
	apt-get install -y apache2
SHELL
```

		- This will basically do what we did manually.
	- Start it up, it will create the new box and do initial configuration and provisioning.
	- If you reload the browser the default Apache page is back.
		- The `index.html` page is still synced but Apache overwrote it.
		- Put back what we had just to test and reload. We should not be back.
- Next we're going to install a full LAMP stack.
	- There's a bunch of stuff to add so rather than include the provisions inline, instead we'll point it at a shell script.

```ruby
# Provision Settings
# config.vm.provision "shell", inline: <<-SHELL
#   apt-get update
#   apt-get install -y apache2
# SHELL

config.vm.provision "shell", path: "bootsrap.sh"
```

	- Create bootstrap.sh and add all of your commands for setting up a full LAMP stack.
		- Note that you do not need to include `sudo` because everything will be run as root.

```bash
# Update Packages
apt-get update
# Upgrade Packages
apt-get upgrade

# Basic Linux Stuff
apt-get install -y git

# Apache
apt-get install -y apache2

# Enable Apache Mods
a2enmod rewrite

#Add Onrej PPA Repo
apt-add-repository ppa:ondrej/php
apt-get update

# Install PHP
apt-get install -y php7.2

# PHP Apache Mod
apt-get install -y libapache2-mod-php7.2

# Restart Apache
service apache2 restart

# PHP Mods
apt-get install -y php7.2-common
apt-get install -y php7.2-mcrypt
apt-get install -y php7.2-zip

# Set MySQL Pass
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

# Install MySQL
apt-get install -y mysql-server

# PHP-MYSQL lib
apt-get install -y php7.2-mysql

# Restart Apache
sudo service apache2 restart
```

- `vagrant destory` and `vagrant up`, this time it should take a while and produce a lot of output.
- Somewhat superfluous content including creating a MySQL database, adding `index.php`, looking at the PHP configuration, etc. Basically just proving that it did what we told it to.